import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class Authentication {
    constructor() {
        this.init();
    }

    init() {
        this.update();
    }

    isAuthenticated = () => {
        return this.authenticated;
    }

    authenticate = (username) => {
        this.user = localStorage.setItem("user", username);
        this.update();
    }

    update() {
        this.user = localStorage.getItem("user")
        this.authenticated = this.user != null;

        return this;
    }

    logout(callback) {
        localStorage.clear();

        if (callback != null) {
            callback();
        }
    }
}

export const withAuth = Component => props => {
    let authStore = new Authentication();

    if (authStore.isAuthenticated() == true) {
        console.log("authenticated")
        return <Component {...props} />
    }

    return <Redirect to="registration" />
}

export const withoutAuth = Component => props => {
    let authStore = new Authentication();

    if (authStore.isAuthenticated() == true) {
        return <Redirect to="/" />
    }

    return <Component {...props} />
}

export default Authentication;