import { createStore } from 'redux';
import appReducers from './reducers';

const store = createStore(appReducers);

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>
)