export const ColorYellow = "#FFC75F";
export const ColorDarkYellow = "#E7B355";
export const ColorWhite = "white";
export const ColorPurple = "#845EC2";

export const ColorGray = "#a0a0a0";
export const ColorDarkGray = "#404040";