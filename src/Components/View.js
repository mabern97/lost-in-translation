import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {Container} from '@material-ui/core';
import styled from 'styled-components';

import Navbar from './Navbar';

const ContainerBox = styled(Container)`
    margin: 0px;
    padding: 0px;
`;

class View extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navTitle: props.title
        };
    }

    render() {
        return(
            <>
                <Navbar history={this.props.history} authStore={this.props.authStore} title={this.state.navTitle} />
                <ContainerBox>
                    {React.cloneElement(this.props.children, {...this.props})}
                </ContainerBox>
            </>
        );
    }
}

export default withRouter(View);