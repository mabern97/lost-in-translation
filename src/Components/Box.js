import styled from 'styled-components';
import { ColorWhite, ColorPurple } from './Colors';

export const Box = styled.div`
    position: absolute;

    left: 50%;
    top: 120%;

    transform: translate(-50%, -120%);

    border-radius: 10px;

    width: 60%;
    height: 120px;

    background-color: ${ColorWhite};
    box-shadow: 0px 0px 10px 0px rgba(0,0,0, 0.5);

    overflow: hidden;
`;

export const BoxAccent = styled.div`
    width: 100%;
    height: 100%;
    box-shadow: inset 0px -20px 0px 0px ${ColorPurple};
`;