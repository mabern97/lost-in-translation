import React, { Component } from 'react';
import { AppBar, Toolbar, IconButton, Typography, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import { red } from '@material-ui/core/colors';

const useStyles = theme => ({
    root: {
        flexGrow: 1,
    },
    menuBar: {
        "background-color": "#FFC75F !important"
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
});

class Navbar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            authStore: props.authStore
        };

        this.onLogout = this.onLogout.bind(this);
    }

    onLogout() {
        const { history } = this.props;
        const { authStore } = this.state;

        authStore.logout(() => {
            this.setState({
                authStore: authStore.update()
            });
            history.push('/profile');
        });
    }

    render() {
        const { classes, title } = this.props;
        const { authStore } = this.props;
        return(
            <>
               <AppBar className={classes.menuBar} position="static">
                   <Toolbar>
                       <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                           <MenuIcon/>
                       </IconButton>

                       <Typography variant="h6" className={classes.title}>
                           { title }
                       </Typography>
                       {authStore.isAuthenticated() && <Button variant="contained" onClick={() => this.onLogout()}>Logout</Button>}
                   </Toolbar>
                </AppBar> 
            </>
        );
    }
}

export default withStyles(useStyles, { withTheme: true })(Navbar);