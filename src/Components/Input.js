import React, { Component } from 'react';
import styled from 'styled-components';
import { ColorWhite, ColorGray, ColorDarkGray } from '../Components/Colors';

const InputBox = styled.input`
    width: ${props => props.width ? props.width : "300px"};
    height: ${props => props.height ? props.height : "50px"};

    background-color: ${ColorWhite};
    border-radius: 10px;

    text-align: center;
    font-size: 1em;
    outline-width: 0;

    border: 2px solid ${ColorGray};

    :focus {
        border: 2px solid ${ColorDarkGray};
    }
`;

class Input extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return(
            <InputBox {...this.props} />
        );
    }
}

export default Input;