import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import logo from './logo.svg';
import './App.css';

import LoginView from './Views/LoginView';
import HomeView from './Views/HomeView';
import ProfileView from './Views/ProfileView';
import ErrorView from './Views/ErrorView';
import View from './Components/View';
import Authentication from './Classes/Authentication';

class Routes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "Lost in Translation",

      authStore: new Authentication(),

      routes: {
        "/": {
          name: "Homepage",
          exact: true,
          view: <HomeView/>
        },

        "/registration": {
            name: "Registration",
            view: <LoginView/>
          },

        "/profile": {
          name: "Profile",
          view: <ProfileView/>
        },

        // Place routes about the *, else the page will be unavailable
        "*": {
          name: "Error",
          view: <ErrorView/>
        }
      }
    };
  }

  mapRoutes() {
    let {routes, title, authStore} = this.state;

    return(
      Object.entries(routes).map(([key, page]) => <Route exact={page.exact} key={key} path={key}>
        <View title={title} authStore={authStore} route={key} name={page.name}>{page.view}</View>
      </Route>)
    );
  }

  render() {
    return(
      <Router>
        <Switch>
          {this.mapRoutes()}
        </Switch>
      </Router>
    );
  }
}

function App() {
  return (
    <>
      <Routes />
    </>
  )
}

export default App;