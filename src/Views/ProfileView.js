import React, { Component } from 'react';
import { withAuth } from '../Classes/Authentication';

class ProfileView extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return(
            <>
                <h2>Profile</h2>
            </>
        );
    }
}

export default withAuth(ProfileView);