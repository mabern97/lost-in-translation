import React, { Component } from 'react';
import { withoutAuth } from '../Classes/Authentication';

import { Box, BoxAccent } from '../Components/Box';
import Input from '../Components/Input';
import { ColorWhite, ColorDarkYellow, ColorYellow } from '../Components/Colors';

import styled from 'styled-components';

const WelcomeBox = styled.div`
    width: 100vw;
    height: 350px;
    position: relative;

    border-top: 3px solid ${ColorDarkYellow};

    background-color: ${ColorYellow};
`;

const WelcomeLogo = styled.img`
    height: 200px;

    position: absolute;
    left: 20%;
    top: 50%;
    transform: translate(-20%, -50%);
`;

const WelcomeTitle = styled.h1`
    position: absolute;
    
    left: 50%;
    top: 20%;
    transform: translateX(-50%);

    color: ${ColorWhite};
`;

const WelcomeSub = styled.h4`
    position: absolute;

    left: 50%;
    top: 40%;
    transform: translate(-50%, -40%);

    color: ${ColorWhite};
`;

const InputStyle = {
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)",
    width: "80%"
};

class LoginView extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <>
            <WelcomeBox>
                <WelcomeLogo src="assets/Logo.png" />
                <WelcomeTitle>Lost in Translation</WelcomeTitle>
                <WelcomeSub>Get started</WelcomeSub>

                <Box>
                    <Input placeholder="Please enter your name" style={InputStyle} type="text" />
                    <BoxAccent/>
                </Box>
            </WelcomeBox>
            </>
        );
    }
}

export default withoutAuth(LoginView);